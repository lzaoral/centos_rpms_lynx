#!/bin/bash

# exit immediately if any command returns non-zero exit code
set -e

# print commands as they are executed by the shell interpreter
set -x

# set locale
export LC_ALL=C
locale

# print basic info about lynx
rpm -q lynx
command -v lynx
lynx --version
ldd /usr/bin/lynx

# iterate over all input HTML files
for tst in data/*.html; do
    # dump their content as plain-text using lynx
    lynx -dump file://localhost${PWD}/${tst} > ${tst}.out

    # drop absolute paths from the output
    sed -e 's|file://.*$||' -i ${tst}.out

    # compare the output with expected output
    diff -u ${tst}.{exp,out}
done
